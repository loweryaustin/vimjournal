#!/bin/bash


runEditor(){
	
	filename=$(date +%b%d%y)
	pageLoc=~/.vimJournal/pages/$filename
	encPageLoc="$pageLoc.gpg"
	if [ -e $encPageLoc ]
	then gpg $encPageLoc
	fi
	date=$(date)
	echo "====================================================================" >> $pageLoc
	echo "Entry Started: $date" >> $pageLoc
	echo "Write about what goals you have acomplished" >> $pageLoc
	vim $pageLoc
	date=$(date)
	echo "Entry Saved: $date" >> $pageLoc
	gpg -e -r loweryaustin@gmail.com $pageLoc
	rm $pageLoc
	cd ~/.vimJournal
	git add .
	git commit -m "Auto Commit $date"
	git push origin master
}

runReader(){
	declare -a entryLocs
	declare -a entryNames
	counter=0
	output=cat
	for entries in ~/.vimJournal/pages/*
	do
		entryLocs[$counter]="$entries"
		fileName="${entries##*/}"
		noExt="${fileName%.*}"
		noEncEntryLocs[$counter]="~/.vimJournal/pages/$noExt"
		((counter++))
		output="$output ~/.vimJournal/pages/$noExt"
		gpg $entries
	done
	${output}
	#for noEncEntries in $noEncEntryLocs
	#do
#		rm $noEncEntries
#	done
	
}
case "$1" in
read)
	runReader
;;
*)
		if [ -d ~/.vimJournal/pages ]
			then runEditor
		else	
			if [ -d ~/.vimJournal ]
				then mkdir pages
			else
				mkdir ~/.vimJournal
				mkdir ~/.vimJournal/pages
			fi
			runEditor
		fi
;;
esac
